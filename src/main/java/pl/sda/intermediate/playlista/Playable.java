package pl.sda.intermediate.playlista;

public abstract class Playable {

    public abstract String play();

}
